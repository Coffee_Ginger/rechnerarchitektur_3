package schneider.thimo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.math.BigInteger;
import java.nio.Buffer;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import static java.lang.Integer.parseInt;
import static java.lang.Integer.toBinaryString;
import static java.lang.Math.pow;

public class cacheSim {

    public static void main(String[] args) throws FileNotFoundException{
        /*cmdUtils cmd =  new cmdUtils();



        System.out.println(Arrays.toString(args));
        try {
            cmd.parseArguments(args);
        } catch(ArrayIndexOutOfBoundsException e){
            throw new Error("Invalid option");
        }
        cmd.printValues();

        //cmd.printOptions();*/
            parseValgrind parser = new parseValgrind("src/trace1.txt",8);
            Cache cache = new Cache(2,20,4);
            result[] results = cache.operateCache(parser.getParsedInstructions());
            int count = 0;
            for(result res: results){

            }
        System.out.println(cache.toString());

    }

}
class Cache {
    private int blockNum;
    private int setNum;
    private int blockSize;
    private Sets[] cache;
    private int hits = 0;
    private int misses = 0;
    private int evictions = 0;

    public Cache(int B, int S, int E) {
        this.blockSize = B;
        this.blockNum = S;
        this.setNum = E;
        this.cache = new Sets[(int) (pow(2, this.blockNum) / this.setNum)];
        System.out.println((int) (pow(2, this.blockNum) / this.setNum));
        System.out.println(cache.length);
        Arrays.fill(cache, new Sets(setNum));
    }

    @Override
    public String toString() {
        return "hits: " + hits + " misses: " + misses + " evictions: " + evictions;
    }

    public result[] operateCache(instructions[] instructions) {
        result[] results = new result[instructions.length];
        int resultCounter = 0;
        int calculatedBlockSize = (int) pow(2, this.blockSize);
        for (instructions inst : instructions) {
            int cacheAddress = Integer.parseInt(inst.getIndex(), 2);
            if (inst.getSize() > calculatedBlockSize) {
                results[resultCounter] = multiAccess(inst, calculatedBlockSize);
            } else {
                results[resultCounter] = singleAccess(inst, cacheAddress);

            }
            resultCounter++;
        }

        return results;
    }

    private result multiAccess(instructions inst, int calculatedBlockSize) {
        int numOfAccesses = (int) Math.ceil((double) inst.getSize() / calculatedBlockSize);
        boolean canOverwrite = true;
        returnValue[] values;
        returnValue finalValue = returnValue.HIT;
        for (int count = 0; count <= numOfAccesses; count++) {

            String address = new BigInteger(inst.getBinaryAddress(), 2).add(new BigInteger(Integer.toString(count))).toString(2);
            String index = new BigInteger(address, 2).mod(new BigInteger(Integer.toBinaryString((int) pow(2, this.blockNum) / this.setNum), 2)).toString(2);


            returnValue value = cache[Integer.parseInt(index, 2)].contained(address.substring(0, address.length() - index.length()));
            if (finalValue.equals(returnValue.HIT)) {
                finalValue = value;
            }

        }
        if (inst.getOperation().equals("M")) {
            if (finalValue.equals(returnValue.EVICTION)) {

                values = new returnValue[3];
                values[0] = returnValue.MISS;
                values[1] = returnValue.EVICTION;
                values[2] = returnValue.HIT;
            } else if (finalValue.equals(returnValue.MISS)) {
                values = new returnValue[2];
                values[0] = returnValue.MISS;
                values[1] = returnValue.HIT;

            } else {
                values = new returnValue[2];
                values[0] = returnValue.HIT;
                values[1] = returnValue.HIT;

            }

        } else {
            if (finalValue.equals(returnValue.EVICTION)) {
                values = new returnValue[2];
                values[0] = returnValue.MISS;
                values[1] = returnValue.EVICTION;

            } else if (finalValue.equals(returnValue.MISS)) {
                values = new returnValue[1];
                values[0] = returnValue.MISS;

            } else {
                values = new returnValue[1];
                values[0] = returnValue.HIT;

            }
        }
        return new result(inst.getLineNumber(), values, inst.getOperation());
            /*System.out.println(inst.getBinaryAddress());
            String address = new BigInteger(inst.getBinaryAddress(),2).add(new BigInteger(Integer.toString(count))).toString(2);
            System.out.println(address);
            String index = new BigInteger(address,2).mod(new BigInteger(Integer.toBinaryString((int)pow(2,this.blockNum)/this.setNum),2)).toString(2);

            returnValue value = cache[Integer.parseInt(index,2)].contained(address.substring(0,address.length()-index.length()));
            if((value.equals(returnValue.EVICTION) || value.equals(returnValue.MISS) && canOverwrite)){
                if(value.equals(returnValue.EVICTION) && inst.getOperation().equals("M")) {
                    returnValue[] values = new returnValue[3];
                    values[0] = returnValue.MISS;
                    values[1] = returnValue.EVICTION;
                    values[2] = returnValue.HIT;
                    this.misses++;
                    this.evictions++;
                    this.hits++;
                    new result(inst.getLineNumber(),values,"M");
                    canOverwrite = false;
                } else if(value.equals(returnValue.EVICTION)){
                    returnValue[] values = new returnValue[2];
                    values[0] = returnValue.MISS;
                    values[1] = returnValue.EVICTION;
                    this.misses++;
                    this.evictions++;
                    new result(inst.getLineNumber(),values,inst.getOperation());
                    canOverwrite = false;
                } else {
                    if(inst.getOperation().equals("M")) {
                        System.out.println("Miss and hit");
                        returnValue[] values = new returnValue[2];
                        values[0] = returnValue.MISS;
                        values[1] = returnValue.HIT;
                        this.misses++;
                        this.hits++;
                        new result(inst.getLineNumber(), values, inst.getOperation());
                        canOverwrite = false;
                    } else {
                        returnValue[] values = new returnValue[1];
                        values[0] = returnValue.MISS;
                        this.misses++;
                        new result(inst.getLineNumber(), values, inst.getOperation());
                        canOverwrite = false;

                    }

                }
            } else {
                if(inst.getOperation().equals("M")) {
                    returnValue[] values = new returnValue[2];
                    values[0] = returnValue.HIT;
                    values[1] = returnValue.HIT;
                    this.hits++;
                    this.hits++;
                    new result(inst.getLineNumber(), values, inst.getOperation());
                } else {
                    returnValue[] values = new returnValue[1];
                    values[0] = returnValue.HIT;
                    this.hits++;
                    new result(inst.getLineNumber(), values, inst.getOperation());

                }

            }
        }*/

    }

    private result singleAccess(instructions inst, int cacheAddress) {
        returnValue[] values;


        returnValue value = cache[cacheAddress].contained(inst.getTag());

        if (inst.getOperation().equals("M")) {
            if (value.equals(returnValue.EVICTION)) {

                values = new returnValue[3];
                values[0] = returnValue.MISS;
                values[1] = returnValue.EVICTION;
                values[2] = returnValue.HIT;
            } else if (value.equals(returnValue.MISS)) {
                values = new returnValue[2];
                values[0] = returnValue.MISS;
                values[1] = returnValue.HIT;

            } else {
                values = new returnValue[2];
                values[0] = returnValue.HIT;
                values[1] = returnValue.HIT;

            }

        } else {
            if (value.equals(returnValue.EVICTION)) {
                values = new returnValue[2];
                values[0] = returnValue.MISS;
                values[1] = returnValue.EVICTION;

            } else if (value.equals(returnValue.MISS)) {
                values = new returnValue[1];
                values[0] = returnValue.MISS;

            } else {
                values = new returnValue[1];
                values[0] = returnValue.HIT;

            }

        }
        return new result(inst.getLineNumber(), values, inst.getOperation());

    }
}
class result {
    private int lineNumber;
    private returnValue[] rV;
    private String operation;
    public result(int lineNumber, returnValue[] rv, String operation){
        this.lineNumber = lineNumber;
        this.operation = operation;
        this.rV = rv;
    }
    public String getOperation(){
        return  this.operation;
    }
    public int getrVLength(){
        return rV.length;
    }
    public String getrVString(){
        return rV.toString();
    }
    public returnValue[] getArray(){
        return this.rV;
    }

    @Override
    public String toString() {
        return "result{" +
                "lineNumber=" + lineNumber +
                ", operation='" + operation + '\'' +
                ", rV=" + Arrays.toString(rV) +
                '}';
    }
}
class Sets {
    private cacheLine[] cacheLines;
    public Sets(int setNum){
        this.cacheLines = new cacheLine[setNum];
        Arrays.fill(cacheLines,new cacheLine());

    }
    public returnValue contained(String tag){
        returnValue value = returnValue.EVICTION;
        for(int count = 0; count<cacheLines.length; count++){
            cacheLine current = cacheLines[count];


            if(cacheLines[count].isValid()){
                if(cacheLines[count].getTag().equals(tag)){
                    cacheLines[count].setTimeStamp();
                    value = returnValue.HIT;
                    return value;
                }
            }

        }
        for(cacheLine block: cacheLines) {
            if (!block.isValid()) {
                value = returnValue.MISS;
                block.writeCacheLine(tag);
                return value;
            }
        }
        int indexOldest = 0;
        long oldest = cacheLines[0].getTimeStamp();
        int counter = 0;
        for(cacheLine block: cacheLines) {
            if(block.getTimeStamp() < oldest){
                indexOldest = counter;
                oldest = block.getTimeStamp();
                System.out.println("in if");

            }
            counter++;

        }
        System.out.println(indexOldest);
        cacheLines[indexOldest].writeCacheLine(tag);
        return returnValue.EVICTION;


    }

}
class cacheLine {
    private String tag;
    private boolean isValid;
    long timeStamp;
    public cacheLine(){
        timeStamp = System.nanoTime();
        this.isValid = false;

    }

    public void writeCacheLine(String tag){
        this.timeStamp = System.nanoTime();
        this.isValid = true;
        this.tag = tag;
    }
    public long getTimeStamp(){
        return this.timeStamp;
    }
    public void setTimeStamp(){
        this.timeStamp = System.nanoTime();
    }
    public boolean isValid(){
        return this.isValid;
    }
    public String getTag(){
        return this.tag;
    }
    public boolean isOlder(int otherTimeStamp){
        return this.timeStamp <= otherTimeStamp ? true : false;
    }
    //public returnValue addressExists(String tag){
//
 //   }

}
enum returnValue {
    HIT,MISS,EVICTION
}
class cmdUtils {
    //indexBits
    private int S;

    private boolean isVerbose;
    //Assoziativität
    private int E;
    //Blockgröße in byte
    private int B;
    //trace file
    private String T;

    private String arguments[] = {"-s","-E","-b","-t"};
    public cmdUtils(){};
    public cmdUtils(int s, int e, int b, String t){

    };
    public String[] getArguments(){
        return this.arguments;
    };
    public boolean getisVerbose(){
        return this.isVerbose;
    }
    public void setisVerbose(boolean v){
        this.isVerbose = v;
    }

    public int getS() {
        return S;
    }

    public void setS(int s) {
        S = s;
    }

    public int getE() {
        return E;
    }

    public void setE(int e) {
        E = e;
    }

    public int getB() {
        return B;
    }

    public void setB(int b) {
        B = b;
    }

    public void printOptions(){
        System.out.println("-s <s>  Anzahl der Indexbits s, S = 2sist die Anzahl der Cache Blöcke");
        System.out.println("-E<E> Assoziativität des Caches, E = Anzahl der Blöcke pro Satz");
        System.out.println("-b <b> Anzahl der Block Bits, B = 2bist die Blockgröße(in Byte)");
        System.out.println("-t <tracefile> der Name der valgrind Trace Datei");
        System.out.println("-v verbose Mode");
        System.out.println("-h zeige diesen hilfe-text");
    }
    public void parseArguments(String args[]){
        if(!Arrays.asList(args).containsAll(Arrays.asList(this.arguments))){
            throw new Error("Wrong Arguments or wrong number of Arguments.-s,-E-b and -t are required");
        }
        int count=0;
        while(count<args.length){
            System.out.println(count);

            if(hasValue(args[count])){
                setValues(args[count],args[count+1]);
                count++;
                count++;
            }else{
                if(args[count].equals("-h")){
                    this.printOptions();
                }
                if(args[count].equals("-v")){
                    this.isVerbose = true;
                }
                count++;
            }


        }
    }

    private void setValues(String flag, String value){
        if(isFlag(value)){
            System.out.println(flag + value);
            throw new Error("Forgot value for " + flag);
        }
        if(isFlag(flag)) {
            if (flag.equals("-t")) {
                this.T = value;
            } else {
                switch (flag) {
                    case "-E":
                        this.E = parseNumber(value, flag);
                        break;

                    case "-s":
                        this.S = parseNumber(value, flag);
                        break;

                    case "-b":
                        this.B = parseNumber(value, flag);
                        break;

                    default:
                        throw new Error("unrecognized argument " + flag);
                }
            }
        }

    }
    private boolean hasValue(String flag){
        if (!this.isFlag(flag)){
            System.out.println(flag);
            throw new Error("You probably forgot a value somewhere");
        }
        if(flag.equals("-v")||flag.equals("-h")){
            return false;
        }
        if(!Arrays.asList(this.arguments).contains(flag)){

            throw new Error("unrecognized Option");
        }
        return true;
    }
    private boolean isFlag(String hopefullyFlag){
        if(hopefullyFlag.length()==2 && hopefullyFlag.contains("-")){
            return true;
        }
        return false;
    }
    private int parseNumber(String hopefullyNum, String flag){
        if(!hopefullyNum.matches("[0-9]+")){
            throw new Error(flag + " requires a number");
        }
        return parseInt(hopefullyNum);
    }

    @Override
    public String toString() {
        return "cmdUtils{" +
                "S=" + S +
                ", isVerbose=" + isVerbose +
                ", E=" + E +
                ", B=" + B +
                ", T='" + T + '\'' +
                ", arguments=" + Arrays.toString(arguments) +
                '}';
    }
}
class parseValgrind {
    private LineNumberReader lineReader;
    private instructions[] parsedInstructions;


    public parseValgrind(String file,int blockNum) throws FileNotFoundException {
        this.lineReader = new LineNumberReader(new FileReader(file));
        String parsedLine;
        boolean keepGoing = true;
        ArrayList<instructions> parsed = new ArrayList<>();
        try {
            while (keepGoing) {
                int lineNumber = lineReader.getLineNumber();
                parsedLine = lineReader.readLine();
                if(parsedLine == null){
                    keepGoing = false;
                }else {
                    if(parsedLine.startsWith(" ")){
                        String[] splitWithSpace;
                        String[] splitWithComma;
                        splitWithSpace = parsedLine.split(" ");
                        splitWithComma = splitWithSpace[2].split(",");
                        String binaryAddress = new BigInteger(splitWithComma[0],16).toString(2);
                        parsed.add(new instructions(splitWithSpace[1],binaryAddress.substring(0,binaryAddress.length()-blockNum), binaryAddress.substring(binaryAddress.length()-blockNum),binaryAddress,Integer.parseInt(splitWithComma[1]),lineNumber));


                    }
                }
            }
            parsedInstructions = parsed.toArray(new instructions[parsed.size()]);
        } catch (Exception e){
            System.out.println(e);
        }
    }

    public instructions[] getParsedInstructions() {
        return parsedInstructions;
    }
}
class instructions {
    private String operation;
    private String tag;
    private String index;
    private String binaryAddress;
    private int size;
    private int lineNumber;

    public instructions(String operation, String tag, String index,String binaryAddress, int size, int lineNumber){
        this.lineNumber = lineNumber;
        this.binaryAddress = binaryAddress;
        this.tag = tag;
        this.index = index;
        this.operation = operation;
        this.size = size;
    }

    public String getOperation() {
        return operation;
    }
    public String getBinaryAddress(){
        return this.binaryAddress;
    }

    public void setModify(String operation) {
        this.operation = operation;
    }

    public String getTag() {
        return tag;
    }
    public String getIndex(){
        return this.index;
    }
    public int getLineNumber(){
        return this.lineNumber;
    }

    public void setAddress(String address) {
        this.tag = tag;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "instructions{" +
                "operation=" + operation +
                ", tag='" + tag + '\'' +
                ", index='" + index + '\'' +
                ", binaryAddress='" + binaryAddress + '\'' +
                ", size=" + size +
                ", lineNumber=" + lineNumber +
                '}';
    }
}
